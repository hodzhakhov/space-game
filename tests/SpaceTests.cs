﻿using System.Collections.Generic;
using src.CompareOfShips;
using src.Results;
using src.Routes;
using src.Space.Environments.Models;
using src.Space.Obstacles.Entities;
using src.Space.Obstacles.Models;
using src.SpaceCrafts.Ships.Entities;
using src.SpaceCrafts.Ships.Models;
using Xunit;

namespace tests.Tests;

public class SpaceTests
{
    [Theory]
    [InlineData("Ship was lost", "Ship was lost")]
    public void ShuttleAndAugurInNebulaeSpace(string expectedResultOfShuttle, string expectedResultOfAugur)
    {
        // Arrange
        var nebulaeSpace = new NebulaeSpace(310, new List<INebulaeObstacle>());
        var route = new Route();
        route.AddEnvironment(nebulaeSpace);
        var shuttle = new PleasureShuttle();
        var augur = new Augur();

        // Act
        Result resultOfShuttle = route.PassRoute(shuttle);
        Result resultOfAugur = route.PassRoute(augur);

        // Assert
        Assert.Equal(expectedResultOfShuttle, resultOfShuttle.ResultText);
        Assert.Equal(expectedResultOfAugur, resultOfAugur.ResultText);
    }

    [Theory]
    [InlineData("Team was killed", "Ship has completed path")]
    public void VaklasAndVaklasWithPhotonicInNebulaeSpace(string expectedResultOfVaklas, string expectedResultOfPhotonicVaklas)
    {
        // Arrange
        var nebulaeSpace = new NebulaeSpace(310, new List<INebulaeObstacle>() { new Antimatter() });
        var route = new Route();
        route.AddEnvironment(nebulaeSpace);
        var vaklas = new Vaklas();
        var vaklasWithPhotonic = new Vaklas();
        vaklasWithPhotonic.MakePhotonicDeflector();

        // Act
        Result resultOfVaklas = route.PassRoute(vaklas);
        Result resultOfPhotonicVaklas = route.PassRoute(vaklasWithPhotonic);

        // Assert
        Assert.Equal(expectedResultOfVaklas, resultOfVaklas.ResultText);
        Assert.Equal(expectedResultOfPhotonicVaklas, resultOfPhotonicVaklas.ResultText);
    }

    [Theory]
    [InlineData("Ship was destroyed", "Ship has completed path but deflector is broken", "Ship has completed path")]
    public void VaklasAndAugurAndMeredianInNitroNebulaeSpace(string expectedResultOfVaklas, string expectedResultOfAugur, string expectedResultOfMeredian)
    {
        // Arrange
        var nitroNebulaeSpace = new NitroNebulaeSpace(200, new List<INitroNebulaeObstacle>() { new SpaceWhale() });
        var route = new Route();
        route.AddEnvironment(nitroNebulaeSpace);
        var vaklas = new Vaklas();
        var augur = new Augur();
        var meredian = new Meredian();

        // Act
        Result resultOfVaklas = route.PassRoute(vaklas);
        Result resultOfAugur = route.PassRoute(augur);
        Result resultOfMeredian = route.PassRoute(meredian);

        // Assert
        Assert.Equal(expectedResultOfVaklas, resultOfVaklas.ResultText);
        Assert.Equal(expectedResultOfAugur, resultOfAugur.ResultText);
        Assert.Equal(expectedResultOfMeredian, resultOfMeredian.ResultText);
    }

    [Theory]
    [InlineData("PleasureShuttle")]
    public void PleasureShuttleAndVaklasInOrdinarySpace(string expectedResultOfCompare)
    {
        // Arrange
        var ordinarySpace = new OrdinarySpace(100, new List<IOrdinarySpaceObstacle>());
        var route = new Route();
        route.AddEnvironment(ordinarySpace);
        var shuttle = new PleasureShuttle();
        var vaklas = new Vaklas();
        var ships = new List<IShip>
        {
            shuttle,
            vaklas,
        };
        var comparator = new Comparator(ships, route);

        // Act
        ResultOfCompare resultOfCompare = comparator.Compare();

        // Assert
        Assert.Equal(expectedResultOfCompare, resultOfCompare.Result.ShipName);
    }

    [Theory]
    [InlineData("Stella")]
    public void AugurAndStellaInNebulaeSpace(string expectedResultOfCompare)
    {
        // Arrange
        var nebulaeSpace = new NebulaeSpace(350, new List<INebulaeObstacle>());
        var route = new Route();
        route.AddEnvironment(nebulaeSpace);
        var augur = new Augur();
        var stella = new Stella();
        var ships = new List<IShip>
        {
            augur,
            stella,
        };
        var comparator = new Comparator(ships, route);

        // Act
        ResultOfCompare resultOfCompare = comparator.Compare();

        // Assert
        Assert.Equal(expectedResultOfCompare, resultOfCompare.Result.ShipName);
    }

    [Theory]
    [InlineData("Vaklas")]
    public void PleasureShuttleAndVaklasInNitorNebulaeSpace(string expectedResultOfCompare)
    {
        // Arrange
        var nitroNebulaeSpace = new NitroNebulaeSpace(200, new List<INitroNebulaeObstacle>());
        var route = new Route();
        route.AddEnvironment(nitroNebulaeSpace);
        var shuttle = new PleasureShuttle();
        var vaklas = new Vaklas();
        var ships = new List<IShip>
        {
            shuttle,
            vaklas,
        };
        var comparator = new Comparator(ships, route);

        // Act
        ResultOfCompare resultOfCompare = comparator.Compare();

        // Assert
        Assert.Equal(expectedResultOfCompare, resultOfCompare.Result.ShipName);
    }

    [Theory]
    [InlineData("Ship has completed path but deflector is broken")]
    public void VaklasOnCustomRoute(string expectedResultOfAugur)
    {
        // Arrange
        var nebulaeSpace = new NebulaeSpace(300, new List<INebulaeObstacle>() { new Antimatter() });
        var nitroNebulaeSpace = new NitroNebulaeSpace(100, new List<INitroNebulaeObstacle>() { new SpaceWhale() });
        var ordinarySpace = new OrdinarySpace(200, new List<IOrdinarySpaceObstacle>() { new Asteroid() });
        var route = new Route();
        route.AddEnvironment(nebulaeSpace);
        route.AddEnvironment(nitroNebulaeSpace);
        route.AddEnvironment(ordinarySpace);
        var augur = new Augur();
        augur.MakePhotonicDeflector();

        // Act
        Result resultOfAugur = route.PassRoute(augur);

        // Assert
        Assert.Equal(expectedResultOfAugur, resultOfAugur.ResultText);
    }
}
