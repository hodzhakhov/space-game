﻿using src.Results;

namespace src.SpaceCrafts.Engines.Entities;

public interface IEngine
{
    public Result Spent(int distance);
}
