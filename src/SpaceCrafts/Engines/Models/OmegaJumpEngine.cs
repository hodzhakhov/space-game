﻿using System;
using src.Markets;
using src.Results;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Fuel.Entities;
using src.SpaceCrafts.Fuel.Models;

namespace src.SpaceCrafts.Engines.Models;

public class OmegaJumpEngine : IJumpEngine
{
    private const int Length = 400;
    private int MaxLength { get; init; } = Length;
    private FuelType Fuel { get; init; } = new GravitonMatter();

    public Result Spent(int distance)
    {
        bool conclusion = MaxLength >= distance;
        var market = new FuelMarket();

        return new Result(conclusion, new RouteResult(market.CountCost(distance * Math.Log(distance), Fuel), distance * Math.Log(distance)), string.Empty, string.Empty);
    }
}
