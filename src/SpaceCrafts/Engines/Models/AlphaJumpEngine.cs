﻿using src.Markets;
using src.Results;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Fuel.Entities;
using src.SpaceCrafts.Fuel.Models;

namespace src.SpaceCrafts.Engines.Models;

public class AlphaJumpEngine : IJumpEngine
{
    private const int Length = 300;
    private int MaxLength { get; init; } = Length;
    private FuelType Fuel { get; init; } = new GravitonMatter();

    public Result Spent(int distance)
    {
        bool conclusion = MaxLength >= distance;
        var market = new FuelMarket();

        return new Result(conclusion, new RouteResult(market.CountCost(distance, Fuel), distance), string.Empty, string.Empty);
    }
}
