﻿using System;
using src.Markets;
using src.Results;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Fuel.Entities;
using src.SpaceCrafts.Fuel.Models;

namespace src.SpaceCrafts.Engines.Models;

public class ImpulseEngineE : IImpulseEngine
{
    private FuelType Fuel { get; init; } = new ActivePlasma();

    public Result Spent(int distance)
    {
        var market = new FuelMarket();
        return new Result(true, new RouteResult(market.CountCost(distance * distance, Fuel), Math.Log(distance + 1)), string.Empty, string.Empty);
    }
}
