﻿namespace src.SpaceCrafts.CaseStrengths.Entities;

public interface ICaseStrength
{
    public double Hp { get; }
    public void TakeDamage(double damage);
}
