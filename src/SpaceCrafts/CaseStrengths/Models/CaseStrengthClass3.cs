﻿using src.SpaceCrafts.CaseStrengths.Entities;

namespace src.SpaceCrafts.CaseStrengths.Models;

public class CaseStrengthClass3 : ICaseStrength
{
    private const double LowDamage = 25;
    private const double MiddleDamage = 50;
    private const double HighDamage = 100;
    private const double LowRatio = 0.2;
    private const double MiddleRatio = 0.8;
    private const double HighRatio = 2;

    public double Hp { get; private set; } = 100.0;

    public void TakeDamage(double damage)
    {
        switch (damage)
        {
            case <= LowDamage:
                Hp -= damage * LowRatio;
                break;
            case <= MiddleDamage:
                Hp -= damage * MiddleRatio;
                break;
            case <= HighDamage:
                Hp -= damage * HighRatio;
                break;
        }
    }
}
