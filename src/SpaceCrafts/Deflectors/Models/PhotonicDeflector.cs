﻿using src.Results;
using src.SpaceCrafts.Deflectors.Entities;

namespace src.SpaceCrafts.Deflectors.Models;

public class PhotonicDeflector : IModifiedDeflector
{
    private readonly IDeflector _deflector;

    public PhotonicDeflector(IDeflector deflector)
    {
        _deflector = deflector;
    }

    public double Hp => _deflector.Hp;

    private double PhotonicHp { get; set; } = 3;

    public DamageResult TakePhotonicDamage()
    {
        if (Hp > 0 && PhotonicHp > 0)
        {
            --PhotonicHp;
            return new DamageResult();
        }

        return new TeamDamageResult();
    }

    public void TakeDamage(double damage)
    {
        _deflector.TakeDamage(damage);
    }
}
