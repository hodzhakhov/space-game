﻿using src.SpaceCrafts.Deflectors.Entities;

namespace src.SpaceCrafts.Deflectors.Models;

public class DeflectorClass1 : IDeflector
{
    private const double LowDamage = 25;
    private const double MiddleDamage = 50;
    private const double HighDamage = 1000;
    private const double LowRatio = 2;
    private const double MiddleRatio = 2;
    private const double HighRatio = 1;

    public double Hp { get; private set; } = 100.0;

    public void TakeDamage(double damage)
    {
        switch (damage)
        {
            case <= LowDamage:
                Hp -= damage * LowRatio;
                break;
            case <= MiddleDamage:
                Hp -= damage * MiddleRatio;
                break;
            case <= HighDamage:
                Hp -= damage * HighRatio;
                break;
        }
    }
}
