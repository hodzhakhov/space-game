﻿using src.Results;

namespace src.SpaceCrafts.Deflectors.Entities;

public interface IModifiedDeflector : IDeflector
{
    public DamageResult TakePhotonicDamage();
}
