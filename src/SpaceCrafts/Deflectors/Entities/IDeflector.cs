﻿namespace src.SpaceCrafts.Deflectors.Entities;

public interface IDeflector
{
    public double Hp { get; }
    public void TakeDamage(double damage);
}
