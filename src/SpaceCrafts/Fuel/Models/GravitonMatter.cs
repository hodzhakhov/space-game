﻿using src.SpaceCrafts.Fuel.Entities;

namespace src.SpaceCrafts.Fuel.Models;

public record GravitonMatter : FuelType;
