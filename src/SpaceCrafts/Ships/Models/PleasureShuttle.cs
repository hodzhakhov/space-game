﻿using src.Results;
using src.SpaceCrafts.CaseStrengths.Entities;
using src.SpaceCrafts.CaseStrengths.Models;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Engines.Models;
using src.SpaceCrafts.Ships.Entities;

namespace src.SpaceCrafts.Ships.Models;

public class PleasureShuttle : IShip
{
    public string Name => "PleasureShuttle";
    public IImpulseEngine ImpulseEngine { get; } = new ImpulseEngineC();
    public ICaseStrength CaseStrength { get; } = new CaseStrengthClass1();
    public bool AntiNitriteEmitter => false;
    private bool CaseIsActive { get; set; } = true;

    public DamageResult TakeDamage(double damage)
    {
        if (CaseIsActive)
        {
            CaseStrength.TakeDamage(damage);
            if (CaseStrength.Hp < 0)
            {
                CaseIsActive = false;
                return new CaseDamageResult();
            }
        }
        else
        {
            return new ShipDamageResult();
        }

        return new DamageResult();
    }
}
