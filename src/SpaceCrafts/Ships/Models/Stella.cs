﻿using src.Results;
using src.SpaceCrafts.CaseStrengths.Entities;
using src.SpaceCrafts.CaseStrengths.Models;
using src.SpaceCrafts.Deflectors.Entities;
using src.SpaceCrafts.Deflectors.Models;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Engines.Models;
using src.SpaceCrafts.Ships.Entities;

namespace src.SpaceCrafts.Ships.Models;

public class Stella : IShipWithJumpEngine, IShipWithDeflector
{
    public string Name => "Stella";
    public IImpulseEngine ImpulseEngine { get; } = new ImpulseEngineC();
    public IJumpEngine JumpEngine { get; } = new OmegaJumpEngine();
    public IDeflector Deflector { get; private set; } = new DeflectorClass1();
    public ICaseStrength CaseStrength { get; } = new CaseStrengthClass1();
    public bool AntiNitriteEmitter => false;
    private bool DeflectorIsActive { get; set; } = true;
    private bool CaseIsActive { get; set; } = true;

    public void MakePhotonicDeflector()
    {
        Deflector = new PhotonicDeflector(Deflector);
    }

    public DamageResult TakeDamage(double damage)
    {
        if (DeflectorIsActive)
        {
            Deflector.TakeDamage(damage);
            if (Deflector.Hp <= -500)
            {
                return new ShipDamageResult();
            }

            if (Deflector.Hp <= 0)
            {
                DeflectorIsActive = false;
                return new DeflectorDamageResult();
            }
        }
        else if (CaseIsActive)
        {
            CaseStrength.TakeDamage(damage);
            if (CaseStrength.Hp < 0)
            {
                CaseIsActive = false;
                return new CaseDamageResult();
            }
        }
        else
        {
            return new ShipDamageResult();
        }

        return new DamageResult();
    }
}
