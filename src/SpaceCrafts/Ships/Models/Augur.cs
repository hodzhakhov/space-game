﻿using src.Results;
using src.SpaceCrafts.CaseStrengths.Entities;
using src.SpaceCrafts.CaseStrengths.Models;
using src.SpaceCrafts.Deflectors.Entities;
using src.SpaceCrafts.Deflectors.Models;
using src.SpaceCrafts.Engines.Entities;
using src.SpaceCrafts.Engines.Models;
using src.SpaceCrafts.Ships.Entities;

namespace src.SpaceCrafts.Ships.Models;

public class Augur : IShipWithJumpEngine, IShipWithDeflector
{
    public string Name => "Augur";
    public IImpulseEngine ImpulseEngine { get; } = new ImpulseEngineE();
    public IJumpEngine JumpEngine { get; } = new AlphaJumpEngine();
    public IDeflector Deflector { get; private set; } = new DeflectorClass3();
    public ICaseStrength CaseStrength { get; } = new CaseStrengthClass3();
    public bool AntiNitriteEmitter => false;
    private bool DeflectorIsActive { get; set; } = true;
    private bool CaseIsActive { get; set; } = true;

    public void MakePhotonicDeflector()
    {
        Deflector = new PhotonicDeflector(Deflector);
    }

    public DamageResult TakeDamage(double damage)
    {
        if (DeflectorIsActive)
        {
            Deflector.TakeDamage(damage);
            if (Deflector.Hp <= -500)
            {
                return new ShipDamageResult();
            }

            if (Deflector.Hp <= 0)
            {
                DeflectorIsActive = false;
                return new DeflectorDamageResult();
            }
        }
        else if (CaseIsActive)
        {
            CaseStrength.TakeDamage(damage);
            if (CaseStrength.Hp < 0)
            {
                CaseIsActive = false;
                return new CaseDamageResult();
            }
        }
        else
        {
            return new ShipDamageResult();
        }

        return new DamageResult();
    }
}
