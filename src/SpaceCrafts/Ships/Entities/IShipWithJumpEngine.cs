﻿using src.SpaceCrafts.Engines.Entities;

namespace src.SpaceCrafts.Ships.Entities;

public interface IShipWithJumpEngine : IShip
{
    public IJumpEngine JumpEngine { get; }
}
