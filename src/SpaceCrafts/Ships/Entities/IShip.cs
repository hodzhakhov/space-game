﻿using src.Results;
using src.SpaceCrafts.CaseStrengths.Entities;
using src.SpaceCrafts.Engines.Entities;

namespace src.SpaceCrafts.Ships.Entities;

public interface IShip
{
    public string Name { get; }
    public IImpulseEngine ImpulseEngine { get; }
    public ICaseStrength CaseStrength { get; }
    public bool AntiNitriteEmitter { get; }

    public DamageResult TakeDamage(double damage);
}
