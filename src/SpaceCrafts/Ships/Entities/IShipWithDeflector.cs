﻿using src.SpaceCrafts.Deflectors.Entities;

namespace src.SpaceCrafts.Ships.Entities;

public interface IShipWithDeflector : IShip
{
    public IDeflector Deflector { get; }
}
