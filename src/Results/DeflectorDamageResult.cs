﻿namespace src.Results;

public record DeflectorDamageResult() : DamageResult;
