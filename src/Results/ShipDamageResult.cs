﻿namespace src.Results;

public record ShipDamageResult() : DamageResult;
