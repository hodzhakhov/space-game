﻿namespace src.Results;

public record Result(bool Conclusion, RouteResult RouteResult, string ShipName, string ResultText);
