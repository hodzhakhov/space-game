﻿namespace src.Results;

public record RouteResult(double SpentMoney, double SpentTime);
