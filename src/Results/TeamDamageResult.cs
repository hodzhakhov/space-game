﻿namespace src.Results;

public record TeamDamageResult() : DamageResult;
