﻿namespace src.Results;

public record ResultOfCompare(Result Result);
