﻿namespace src.Results;

public record CaseDamageResult() : DamageResult;
