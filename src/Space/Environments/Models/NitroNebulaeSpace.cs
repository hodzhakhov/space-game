﻿using System.Collections.Generic;
using src.Results;
using src.Space.Environments.Entities;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Engines.Models;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Environments.Models;

public class NitroNebulaeSpace : IEnvironment
{
    private readonly int _distance;
    public NitroNebulaeSpace(int distance, IEnumerable<INitroNebulaeObstacle> obstacles)
    {
        _distance = distance;
        Obstacles = obstacles;
    }

    public IEnumerable<IObstacle> Obstacles { get; }

    public Result PassEnvironment(IShip ship)
    {
        if (ship.ImpulseEngine is ImpulseEngineE impulseEngineE)
        {
            return impulseEngineE.Spent(_distance);
        }

        return new Result(false, new RouteResult(0, 0), string.Empty, string.Empty);
    }
}
