﻿using System.Collections.Generic;
using src.Results;
using src.Space.Environments.Entities;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Environments.Models;

public class OrdinarySpace : IEnvironment
{
    private readonly int _distance;
    public OrdinarySpace(int distance, IEnumerable<IOrdinarySpaceObstacle> obstacles)
    {
        _distance = distance;
        Obstacles = obstacles;
    }

    public IEnumerable<IObstacle> Obstacles { get; }

    public Result PassEnvironment(IShip ship)
    {
        return ship.ImpulseEngine.Spent(_distance);
    }
}
