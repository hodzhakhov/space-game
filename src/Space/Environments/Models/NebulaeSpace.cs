﻿using System.Collections.Generic;
using src.Results;
using src.Space.Environments.Entities;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Environments.Models;

public class NebulaeSpace : IEnvironment
{
    private readonly int _distance;
    public NebulaeSpace(int distance, IEnumerable<INebulaeObstacle> obstacles)
    {
        _distance = distance;
        Obstacles = obstacles;
    }

    public IEnumerable<IObstacle> Obstacles { get; }

    public Result PassEnvironment(IShip ship)
    {
        if (ship is IShipWithJumpEngine shipWithJumpEngine)
        {
            return shipWithJumpEngine.JumpEngine.Spent(_distance);
        }

        return new Result(false, new RouteResult(0, 0), string.Empty, string.Empty);
    }
}
