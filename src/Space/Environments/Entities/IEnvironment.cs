﻿using System.Collections.Generic;
using src.Results;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Environments.Entities;

public interface IEnvironment
{
    public IEnumerable<IObstacle> Obstacles { get; }

    public Result PassEnvironment(IShip ship);
}
