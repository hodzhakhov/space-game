﻿using src.Results;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Deflectors.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Obstacles.Models;

public class Antimatter : INebulaeObstacle
{
    public DamageResult DoDamage(IShip ship)
    {
        if (ship is IShipWithDeflector shipWithDeflector)
        {
            if (shipWithDeflector.Deflector is IModifiedDeflector modifiedDeflector)
            {
                return modifiedDeflector.TakePhotonicDamage();
            }
        }

        return new TeamDamageResult();
    }
}
