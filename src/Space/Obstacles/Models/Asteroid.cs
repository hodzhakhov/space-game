﻿using src.Results;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Obstacles.Models;

public class Asteroid : IOrdinarySpaceObstacle
{
    private const double Damage = 25.0;
    public DamageResult DoDamage(IShip ship)
    {
        return ship.TakeDamage(Damage);
    }
}
