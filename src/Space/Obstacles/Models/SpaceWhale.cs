﻿using src.Results;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Obstacles.Models;

public class SpaceWhale : INitroNebulaeObstacle
{
    private const double Damage = 1000.0;

    public DamageResult DoDamage(IShip ship)
    {
        if (ship.AntiNitriteEmitter)
        {
            return new DamageResult();
        }

        return ship.TakeDamage(Damage);
    }
}
