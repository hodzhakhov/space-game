﻿using src.Results;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Obstacles.Models;

public class Meteorite : IOrdinarySpaceObstacle
{
    private const double Damage = 50.0;

    public DamageResult DoDamage(IShip ship)
    {
        return ship.TakeDamage(Damage);
    }
}
