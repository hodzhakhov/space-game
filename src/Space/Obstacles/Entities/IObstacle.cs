﻿using src.Results;
using src.SpaceCrafts.Ships.Entities;

namespace src.Space.Obstacles.Entities;

public interface IObstacle
{
    public DamageResult DoDamage(IShip ship);
}
