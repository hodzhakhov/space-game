﻿using src.SpaceCrafts.Fuel.Entities;

namespace src.Markets;

public interface IMarket
{
    public double CountCost(double amountOfFuel, FuelType fuelType);
}
