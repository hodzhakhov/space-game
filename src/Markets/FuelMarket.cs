﻿using src.SpaceCrafts.Fuel.Entities;
using src.SpaceCrafts.Fuel.Models;

namespace src.Markets;

public class FuelMarket : IMarket
{
    private const double ActivePlasmaPrise = 1;
    private const double GravitonMatterPrise = 2;

    public double CountCost(double amountOfFuel, FuelType fuelType)
    {
        return fuelType switch
        {
            ActivePlasma => amountOfFuel * ActivePlasmaPrise,
            GravitonMatter => amountOfFuel * GravitonMatterPrise,
            _ => 0,
        };
    }
}
