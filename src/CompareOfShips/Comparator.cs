﻿using System.Collections.Generic;
using System.Linq;
using src.Results;
using src.Routes;
using src.SpaceCrafts.Ships.Entities;

namespace src.CompareOfShips;

public class Comparator
{
    private readonly IEnumerable<IShip> _ships;
    private readonly Route _route;

    public Comparator(IEnumerable<IShip> ships, Route route)
    {
        _ships = ships;
        _route = route;
    }

    public ResultOfCompare Compare()
    {
        int counter = 0;
        IEnumerable<Result> results = new List<Result>();
        foreach (IShip ship in _ships)
        {
            Result result = _route.PassRoute(ship);
            if (result.Conclusion)
            {
                results = results.Append(result);
                counter++;
            }
        }

        if (counter > 0)
        {
            results = results.OrderBy(result => result.RouteResult.SpentMoney);
            return new ResultOfCompare(results.First());
        }

        return new ResultOfCompare(new Result(false, new RouteResult(0, 0), "No one", string.Empty));
    }
}
