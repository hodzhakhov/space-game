﻿using System.Collections.Generic;
using System.Linq;
using src.Results;
using src.Space.Environments.Entities;
using src.Space.Obstacles.Entities;
using src.SpaceCrafts.Ships.Entities;

namespace src.Routes;

public class Route
{
    private IEnumerable<IEnvironment> _environments = new List<IEnvironment>();

    public void AddEnvironment(IEnvironment environment)
    {
        _environments = _environments.Append(environment);
    }

    public Result PassRoute(IShip ship)
    {
        double spentMoney = 0;
        double spentTime = 0;
        bool deflector = true;
        foreach (IEnvironment environment in _environments)
        {
            foreach (IObstacle obstacle in environment.Obstacles)
            {
                DamageResult damageResult = obstacle.DoDamage(ship);
                if (damageResult is ShipDamageResult)
                {
                    return new Result(false, new RouteResult(0, 0), ship.Name, "Ship was destroyed");
                }

                if (damageResult is TeamDamageResult)
                {
                    return new Result(false, new RouteResult(0, 0), ship.Name, "Team was killed");
                }

                if (damageResult is DeflectorDamageResult)
                {
                    deflector = false;
                }
            }

            Result result = environment.PassEnvironment(ship);
            if (result.Conclusion == false)
            {
                return new Result(false, new RouteResult(0, 0), ship.Name, "Ship was lost");
            }

            spentMoney += result.RouteResult.SpentMoney;
            spentTime += result.RouteResult.SpentTime;
        }

        if (!deflector)
        {
            return new Result(true, new RouteResult(spentMoney, spentTime), ship.Name, "Ship has completed path but deflector is broken");
        }

        return new Result(true, new RouteResult(spentMoney, spentTime), ship.Name, "Ship has completed path");
    }
}
